import React, { Component } from 'react';
import './Phonebook.css';

class Phonebook extends Component {
  constructor(props) {
    super(props);

    let list = JSON.parse(localStorage.getItem('list')) || [];

    let message_id = 0;
    let list_id = 0;
    if (list.length)
      list_id = Math.max.apply(Math, list.map(item => item.id));

    this.buff = {
      page: 'list',
      message_id: message_id,
      list_id: list_id,
    }

    this.state = {
      // form
      selectedId: null,
      phone: '',
      fullname: '',

      // list
      messages: [],
      list: list,
    }

    this.renderForm = this.renderForm.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderList = this.renderList.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
    this.renderMessages = this.renderMessages.bind(this);

    this.addMessage = this.addMessage.bind(this);
    this.removeMessage = this.removeMessage.bind(this);

    this.addItem = this.addItem.bind(this);
    this.editItem = this.editItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.saveItem = this.saveItem.bind(this);
    this.cancelItem = this.cancelItem.bind(this);

    this.eventAddItem = this.eventAddItem.bind(this);
    this.eventEditItem = this.eventEditItem.bind(this);
    this.eventRemoveItem = this.eventRemoveItem.bind(this);
    this.eventSaveItem = this.eventSaveItem.bind(this);
    this.eventCancelItem = this.eventCancelItem.bind(this);
    this.eventChangePhone = this.eventChangePhone.bind(this);
    this.eventChangeFullname = this.eventChangeFullname.bind(this);
  }


  addMessage(text, cl) {
    text = ''+text;
    cl = cl || 'success';
    cl = ''+cl;

    if (!text)
      return;

    this.buff.message_id++;
    let id = this.buff.message_id;

    let messages = this.state.messages;
    messages.push({
      id: id,
      cl: cl,
      text: text
    });

    let cb = this.removeMessage.bind(null, id);

    this.setState({
      ...this.state,
      messages: messages
    }, () => setTimeout(cb, 3000));
  }
  removeMessage(id) {
    let messages = this.state.messages.filter(item => (item.id !== id));

    this.setState({
      ...this.state,
      messages: messages
    });
  }


  addItem() {
    this.buff.page = 'form';

    this.setState({
      ...this.state,
      selectedId: null,
      phone: '',
      fullname: '',
    });
  }
  editItem(id) {
    let item = this.state.list.filter(item => (item.id === id));
    if (!item.length)
      return;

    item = item[0];

    this.buff.page = 'form';

    this.setState({
      ...this.state,
      selectedId: item.id,
      phone: item.phone,
      fullname: item.fullname,
    });
  }
  saveItem() {
    let list = this.state.list;

    let index = -1;
    if (this.state.selectedId)
      index = list.findIndex(item => (item.id === this.state.selectedId));

    if (-1 !== index) {
      list[index].phone = this.state.phone;
      list[index].fullname = this.state.fullname;

    } else {
      this.buff.list_id++;
      let id = this.buff.list_id;

      list.push({
        'id': id,
        'phone': this.state.phone,
        'fullname': this.state.fullname
      });

    }

    this.buff.page = 'list';
    this.addMessage('Успешно сохранено');

    localStorage.setItem('list', JSON.stringify(list));

    this.setState({
      ...this.state,
      selectedId: null,
      list: list,
    });
  }
  removeItem(id) {
    let list = this.state.list.filter(item => (item.id !== id));
    if (list.length === this.state.list.length)
      return;

    this.buff.page = 'list';
    this.addMessage('Успешно удалено');

    localStorage.setItem('list', JSON.stringify(list));

    this.setState({
      ...this.state,
      selectedId: null,
      list: list,
    });
  }
  cancelItem(id) {
    this.buff.page = 'list';

    this.setState({
      ...this.state,
      selectedId: null,
    });
  }


  eventAddItem(e) {
    this.addItem();
  }
  eventEditItem(e) {
    let id = +e.currentTarget.dataset.id;
    if (!id)
      return;

    this.editItem(id);
  }
  eventSaveItem(e) {
    this.saveItem();
  }
  eventRemoveItem(e) {
    let id = +e.currentTarget.dataset.id;
    if (!id)
      return;

    this.removeItem(id);
  }
  eventCancelItem(e) {
    this.cancelItem();
  }

  eventChangePhone(e) {
    let v = e.currentTarget.value;
    setTimeout(() => {
      this.setState({
        ...this.state,
        phone: v,
      });
    }, 1);
  }
  eventChangeFullname(e) {
    let v = e.currentTarget.value;

    setTimeout(() => {
      this.setState({
        ...this.state,
        fullname: v,
      });
    }, 1);
  }

  renderForm() {
    let col25 = {
      width: '25%',
    };
    let col75 = {
      width: '75%',
    };

    return (
      <form className="phonebook__form">
        <table className="table table-stripped table-dark phonebook__table">
          <thead>
            <tr>
              <th className="phonebook__td" style={col25}>Name</th>
              <th className="phonebook__td" style={col75}>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="phonebook__td" style={col25}>Phone</td>
              <td className="phonebook__td" style={col75}><input type="text" value={this.state.phone} onChange={this.eventChangePhone} className="form-control" placeholder="+375 33 656 26 80" /></td>
            </tr>
            <tr>
              <td className="phonebook__td" style={col75}>Fullname</td>
              <td className="phonebook__td" style={col75}><input type="text" value={this.state.fullname} onChange={this.eventChangeFullname} className="form-control" placeholder="Andrey Pestryakov" /></td>
            </tr>
            <tr>
              <td className="phonebook__td" colSpan="2">
                <button type="button" className="btn btn-success btn-small mr-1" onClick={this.eventSaveItem}>Save</button>
                <button type="button" className="btn btn-danger btn-small" onClick={this.eventCancelItem}>Cancel</button>
              </td>
            </tr>
          </tbody>
        </table>
      </form>
    );
  }

  renderMessage(item) {
    return (
      <div key={item.id} className={'alert alert-' + item.cl}>{item.text}</div>
    );
  };

  renderMessages(item) {
    let content = this.state.messages.map(item => {
      return this.renderMessage(item);
    });

    return (
      <div className="phonebook__messages">
        {content}
      </div>
    );
  };

  renderItem(item) {
    let col10 = {
      width: '10%',
    };
    let col25 = {
      width: '25%',
    };
    let col40 = {
      width: '40%',
    };

    return (
      <tr key={item.id} className="phonebook__item">
        <td className="phonebook__td" style={col10}>{item.id}</td>
        <td className="phonebook__td" style={col25}>{item.phone}</td>
        <td className="phonebook__td" style={col25}>{item.fullname}</td>
        <td className="phonebook__td" style={col40}>
          <div className="btn-toolbar">
            <button data-id={item.id} type="button" className="btn btn-primary btn-small mr-1" onClick={this.eventEditItem}>Edit</button>
            <button data-id={item.id} type="button" className="btn btn-danger btn-small" onClick={this.eventRemoveItem}>Delete</button>
          </div>
        </td>
      </tr>
    );
  };

  renderList() {
    let col25 = {
      width: '25%',
    };
    let col10 = {
      width: '10%',
    };
    let col40 = {
      width: '40%',
    };

    let content = this.state.list.map(item => {
      return this.renderItem(item);
    });

    return (
      <div className="phonebook__list">
        <table className="table table-stripped table-dark phonebook__table">
          <thead>
            <tr>
              <th className="phonebook__td" style={col10}>#</th>
              <th className="phonebook__td" style={col25}>Phone</th>
              <th className="phonebook__td" style={col25}>Fullname</th>
              <th className="phonebook__td" style={col40}>Actions</th>
            </tr>
          </thead>
          <tbody>
            {content}
          </tbody>
        </table>
        <div className="phonebook__actions">
          <button className="btn btn-primary" onClick={this.eventAddItem}>Add item</button>
        </div>
      </div>
    );
  };

  render() {
    let page;
    switch (this.buff.page) {
      case 'form':
        page = this.renderForm();
        break;

      default:
      case 'list':
        page = this.renderList();
        break;
    }

    let messages = this.renderMessages();

    return (
      <div className="phonebook">
        {messages}
        {page}
      </div>
    );
  };
}

export default Phonebook;