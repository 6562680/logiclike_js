import React, { Component } from 'react';

import Phonebook from './Phonebook';

class App extends Component {
  render() {
    return (
      <div className="container">
        <h1>Phonebook without UI</h1>
        <div className="App">
          <Phonebook />
        </div>
      </div>
    );
  }
}

export default App;
